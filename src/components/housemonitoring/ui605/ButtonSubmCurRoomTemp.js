import React from 'react';
import connect from "react-redux/es/connect/connect";
import {fetchRooms} from "../../../actions/actionsListOfRooms610";
import UI605CurTemperatureRoom from "../../housemonitoring/ui605/UI605CurTemperatureRoom";
import Button from "antd/es/button";

class ButtonSubmCurRoomTemp extends React.Component {


    handleClick = (roomName) => {
        this.props.onSubmit(roomName);
    }

    render() {
        const {data} = this.props.rooms;

        return (
            <div>
            <Button> onclick ={this.props.handleClick}> Click Me </Button>
            <h4>{data}</h4>
            </div>
        )
    }
}

export default ButtonSubmCurRoomTemp;